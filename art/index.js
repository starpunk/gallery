<!--

// credit: https://www.catswhocode.com/blog/using-keyboard-shortcuts-in-javascript

// var isCtrl = false;

// rem: keycode 39 = right arrow | keycode 40 = down arrow 
// rem: keycode 37 = left arrow | keycode 38 = up arrow

// document.onkeyup=function(e)
// {
//  if(e.which == 17) isCtrl=false;
// }

// ---

function show_subgallery_image_in_main_pane(image_ordinal, subgallery_image_url)
{
  // set the item ID for the main image display.
  main_image_id = 'image' + image_ordinal + '_image';

  // update the page location to display the current image.
  // alert('main_image_id: ' + main_image_id);
  document.getElementById(main_image_id).style.background = eval("'url(" + subgallery_image_url + ")'");
}


function show_main_image_in_main_pane(image_ordinal, main_image_url)
{
  // set the item ID for the main image display.
  main_image_id = 'image' + image_ordinal + '_image';

  // update the page location to display the current image.
  // alert('main_image_id: ' + main_image_id);
  document.getElementById(main_image_id).style.background = eval("'url(" + main_image_url + ")'");
  document.getElementById(main_image_id).style.backgroundSize="100% 100%";
}

//--> 